<?php

use Illuminate\Database\Seeder;
use App\ImagesAds;

class ImageAdsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $object1 = new ImagesAds;
        $object1->title="first image";
        $object1->TargetURL ='test@gmail.com';
        $object1->imagePath ='public/storage/16196409511618833911887.png';
        $object1->save();

         $object2 = new ImagesAds;
        $object2->title="Product image";
        $object2->imagePath ='public/storage/16196397921618833911887.png';
        $object2->save();
    }
}
