<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;



class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
      $user =  User::create([
            'name' => "marah",
            'email' => 'marah633@gmail.com',
            'password' => Hash::make('12345'),
        ]);
        $user->assignRole('admin');
    }
}
