<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolsPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role = Role::create(['name' => 'admin']);
        $permission1 = Permission::create([ 'name' => 'add imageAds']);
        $role->givePermissionTo($permission1);
        $permission1->assignRole($role);

        $permission2 = Permission::create([ 'name' => 'edit imageAds']);
        $role->givePermissionTo($permission2);
        $permission2->assignRole($role);

        $permission3 = Permission::create([ 'name' => 'delete imageAds']);
        $role->givePermissionTo($permission3);
        $permission3->assignRole($role);

        $permission4 = Permission::create([ 'name' => 'view imageAds']);
        $role->givePermissionTo($permission4);
        $permission4->assignRole($role);
    }
}
