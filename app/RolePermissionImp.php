<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class RolePermissionImp 
{
    public function getPermissionNames($user) {
        return $user->getPermissionNames(); 
    }
   public function getPermissions($user) {
        return $user->permissions; 
    }
    public function getRoleNames($user) {
        return $user->getRoleNames();
    }
    public function getUsersRole($roleName) {
        return  User::role($roleName)->get();
    }
    public function getUsersPermission($permissionName) {
        return User::permission($permissionName)->get();
    }
}
