<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller ;
use App\Http\Controllers\ImagesAdsController;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\LoginRequest;
use App\User;
use Session;


class userController extends Controller
{
    use AuthenticatesUsers;

    public function loginSubmit(LoginRequest $request) {
       $validated = $request->validated();
    $email = $request->input('email');
    $password = $request->input('password');
    $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' :"";
    if(auth()->attempt(array($fieldType => $email, 'password' => $password)))
     { 
    //    $images = ImagesAdsController::getImagesAdsAll();
       return redirect()->route('dashboardPage');
        // return view('dashboard_layout',['images'=>$images]);
    }
    else {
        Session::flash('message', 'Email or/and password incorrect');
        return redirect()->back();
        }
      
    }
}
