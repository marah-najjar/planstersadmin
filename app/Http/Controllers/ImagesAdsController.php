<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;


use App\Http\Controllers\Controller ;
use App\Http\Requests\AddImageAdRequest;
use App\Http\Requests\DeleteImageRequest;
use App\Http\Requests\updateImageRequest;
use App\Http\Resources\ImageAdObjResource;
use App\Http\Resources\ImagesAdsCollection;
use App\Http\Resources\GeneralResponsApi;
use App\ImagesAds;

use Session;

class ImagesAdsController extends Controller
{
    
    public function getImagesAdsAll() {
       return  view('dashboard_layout',['images'=> ImagesAds::all()]);
     
    }
    public function getImagesAdsApi() {
       $images = ImagesAds::all();
       return new GeneralResponsApi(['status'=>1 , 'message'=>'' , new ImagesAdsCollection( $images)]);
    }
    public function addImage(AddImageAdRequest $request){
        $validated = $request->validated();
        $image       = $request->file('filename');
        $filename    = time() . $image->getClientOriginalName();
        $image->move(public_path().'/public/storage/',$filename);

        $image_resize = Image::make(public_path().'/public/storage/'.$filename);
        $image_resize->fit(900, 600);
        $image_resize->save(public_path('public/storage/' .$filename));
        $Imagepath = 'public/storage/'.$filename;

          $image = new ImagesAds;

        $image->title = $request->input('title');
        $image->imagePath = $Imagepath;
        if($request->input('targetUrl')!=''){
        $image->TargetURL = $request->input('targetUrl');
        }
        else
        {$image->TargetURL = null;}

        $image->save();
        Session::flash('success', 'created image ad successfully');
        return redirect()->route('dashboardPage');
    }
    public function deleteImage(DeleteImageRequest $request) {
        // var_dump(['id'=> $request->idImage]); die();
        $Image = ImagesAds::find( $request->idImage );
        $Image->delete();

         Session::flash('success', 'deleted image ad successfully');
        return redirect()->route('dashboardPage');
    }
    public function updateImage(updateImageRequest $request) {
        if( $request->input['imageFile'] ) {
             $image       = $request->file('imageFile');
        $filename    = time() . $image->getClientOriginalName();
        $image->move(public_path().'/public/storage/',$filename);

        $image_resize = Image::make(public_path().'/public/storage/'.$filename);
        $image_resize->fit(900, 600);
        $image_resize->save(public_path('public/storage/' .$filename));
        $Imagepath = 'public/storage/'.$filename;
         ImagesAds::where('id', $request->input['idImageUpdate'])
      ->update(['title' => $request->input['titleUpdate'] , 'TargetURL' => $request->input['targetUrlUpdate'] ,'imagePath' =>$Imagepath]);
    
        }
        else {
             ImagesAds::where('id', $request->input['idImageUpdate'])
      ->update(['title' => $request->input['titleUpdate'] , 'TargetURL' => $request->input['targetUrlUpdate'] ]);
        }
         Session::flash('success', 'deleted image ad successfully');
        return redirect()->route('dashboardPage');
      }
}
