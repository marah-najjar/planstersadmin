<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImagesAds extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     use SoftDeletes;
     protected $table = 'images_ads';
     protected $primaryKey = 'id';
    protected $fillable = [
        'id' , 'title', 'imagePath', 'TargetURL',
    ];
}
