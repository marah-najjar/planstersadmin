<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title>Material Design for Bootstrap</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" />
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" />
  <!-- MDB -->
  <link rel="stylesheet" href="{{asset('css/mdb.min.css')}}" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- Custom styles -->
  <link rel="stylesheet" href="{{asset('css/admin.css')}}" />

  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
</head>

<body>
  <!--Main Navigation-->
  <header>
    <!-- Sidebar -->
    <nav id="sidebarMenu" class="collapse d-lg-block sidebar collapse bg-white">
      <div class="position-sticky">
        <div class="list-group list-group-flush mx-3 mt-4">
          <a href="{{ route('dashboardPage') }}" class="list-group-item list-group-item-action py-2 ripple" aria-current="true">
            <i class="fas fa-table fa-fw me-3"></i><span>Slider Images Ads</span>
          </a>
        </div>
      </div>
    </nav>
    <!-- Sidebar -->

    <!-- Navbar -->
    <nav id="main-navbar" class="navbar navbar-expand-lg navbar-light bg-white fixed-top">
      <!-- Container wrapper -->
      <div class="container-fluid">
        <!-- Toggle button -->
        <button class="navbar-toggler" type="button" data-mdb-toggle="collapse" data-mdb-target="#sidebarMenu"
          aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
          <i class="fas fa-bars"></i>
        </button>

        <!-- Brand -->
        <a class="navbar-brand" href="#">
          <img src="https://mdbootstrap.com/img/logo/mdb-transaprent-noshadows.png" height="25" alt="" loading="lazy" />
        </a>
      </div>
      <!-- Container wrapper -->
    </nav>
    <!-- Navbar -->
  </header>
  <!--Main Navigation-->

  <!--Main layout-->
  <main style="margin-top: 58px">
    <div class="container pt-4">
      <!-- Section: Table -->
      <section class="mb-4">
        <div class="card">
          <div class="card-header py-3">
            <h5 class="mb-0 text-center"><strong>Slider Images Ads</strong></h5>        
                <button id="addBtn" type="button" class="btn btn-primary" data-target="#addModal" style="float : right">
                    Add Image Ad
                </button>
          </div>
          <div class="card-body">
         <div class="table-responsive">
            <table class="table">
                <caption>List of images</caption>
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">title</th>
      <th scope="col">image</th>
      <th scope="col">target URL</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
      @foreach($images  as  $value)
    <tr>
      <th scope="row">{{$value['id']}}</th>
      <td>{{$value['title']}}</td>
      <td><img src= "{{URL::asset($value['imagePath']) }}" style ="width : 3rem ; height : 3rem;"> </td>
      <td>@if($value['TargetURL']!= null)  {{$value['TargetURL']}} @else  No URL @endif </td>
      <td>
        
           <button id="EditBtn_{{$value['id']}}" onclick="EditRecord({{$value['id']}})" data-id="{{$value['id']}}" data-title="{{$value['title']}} " data-url="{{$value['targetURL']}}" data-path="{{$value['imagePath']}}" data-target="updateModal" type="button" class="btn btn-warning"> Edit</button>
           <button  id="deleteRecord" type="button" onclick="DeleteModal({{$value['id']}})" class="btn btn-danger"> Delete</button>
      </td>
    </tr>
    @endforeach
  </tbody>
            </table>
            </div>
          </div>
        </div>
      </section>
      <!-- Section: Table -->

    </div>
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addModal">add Image Ad</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="post" action="{{ route('addImage') }}" enctype="multipart/form-data">
    @csrf
    <div class="col-md-12">
        <label>Image file <label style="color : red;">*</label>:</label>
          <div class="col-md-12"></div>
          <div class="form-group col-md-12">
          <input type="file" name="filename" class="form-control" required>
          </div>
        </div>
        <div class="col-md-12 form-group">
        <label>Image Title <label style="color : red;">*</label>:</label>            
      <input type="text" id="title" class="form-control" name="title" required placeholder="Degitl product">
      </div>
      <div class="col-md-12 form-group">
        <label>Target URL :</label>          
      <input type="email" id="targetUrl" class="form-control" name="targetUrl" placeholder="www.exampleSite.com">
      </div>
      </div>
      <div class="modal-footer">
        <button  type="submit" class="btn btn-primary">Create</button>
      </div>
      </form>
    </div>
  </div>
</div>

  <div id="deleteConfirmationImage" class="modal" tabindex="-1" role="dialog" aria-labelledby="deleteConfirmationImage" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to DELETE this image ad ?</p>
      </div>
      <div class="modal-footer">
       <form method="post" action="{{ route('deleteImage') }}" enctype="multipart/form-data">
          @csrf
           <input hidden  name="idImage" id="idImage">          
        <button  type="submit" class="btn btn-danger">Delete</button>
          </form>
        
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addModal">Update Image Ad</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="post" action="{{ route('updateImage') }}" enctype="multipart/form-data">
    @csrf
    <div class="col-md-12">
        <label>Image file <label style="color : red;">*</label>:</label>
          <div class="col-md-12"></div>
          <div class="form-group col-md-12">
          <input type="file" id="imageFile" name="imageFile" class="form-control" >
          </div>
        </div>
        <div class="col-md-12 form-group">
        <label>Image Title <label style="color : red;">*</label>:</label>            
      <input type="text" id="titleUpdate" class="form-control" name="title" required placeholder="Degitl product">
      </div>
      <div class="col-md-12 form-group">
        <label>Target URL :</label>          
      <input type="text" id="targetUrlUpdate" class="form-control" name="targetUrl" placeholder="www.exampleSite.com">
      </div>
      <input hidden id="idImageUpdate" >
      </div>
      <div class="modal-footer">
        <button  type="submit" class="btn btn-primary">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>
  </main>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script>
$('#addBtn').on('click', function () {
   $("#addModal").modal();
});
function DeleteModal(id) {
  document.getElementById('idImage').value =id;
   $("#deleteConfirmationImage").modal();
  
}
function EditRecord(id) {
  // var _self = $(this);
// console.log(document.getElementById("EditBtn_"+id).getAttribute('data-id'));
  document.getElementById('imageFile').setAttribute("value",document.getElementById("EditBtn_"+id).getAttribute('data-path') );
  document.getElementById('titleUpdate').setAttribute("value",document.getElementById("EditBtn_"+id).getAttribute('data-title') );
  document.getElementById('targetUrlUpdate').setAttribute("value",document.getElementById("EditBtn_"+id).getAttribute('data-url') != null ?document.getElementById("EditBtn_"+id).getAttribute('data-url'): '' );
  document.getElementById('idImageUpdate').setAttribute("value",document.getElementById("EditBtn_"+id).getAttribute('data-id') );
   $("#updateModal").modal();
}


    </script>
    <script>
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "100",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "show",
            "hideMethod": "hide"
        };
         @if ( Session::has('success') )
         toastr.success( ' {{ Session::get("success") }}');
         @endif

          @if ( Session::has('error') )
         toastr.error(  "{{ Session::get('error') }}");
         @endif

        </script>
</body>

</html>