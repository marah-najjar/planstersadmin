
<!--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">-->
@include('header')
       <div class="wrapper fadeInDown">
         @if ( Session::has('message') )
<div class="alert alert-danger">
    {{ Session::get('message') }}
</div>
@endif
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
           <h2>Login</h2>
    </div>
    
    <!-- Login Form -->
    <form method="post" action="{{ route('loginSubmit') }}" enctype="multipart/form-data">
    @csrf
      <input type="text" id="email" class="fadeIn second" name="email" placeholder="example@gmail.com">
      <input type="text" id="password" class="fadeIn third" name="password" placeholder="12345">
      <input type="submit" class="fadeIn fourth" value="Log In">
    </form>

  </div>
</div>
@include('footer')
