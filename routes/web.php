<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::get('dashboardPage', array('uses' => 'ImagesAdsController@getImagesAdsAll'))->name('dashboardPage')->middleware('admin');

Route::post('loginSubmit', array('uses' => 'userController@loginSubmit'))->name('loginSubmit');
Route::post('addImage', array('uses' => 'ImagesAdsController@addImage'))->name('addImage')->middleware('admin');
Route::post('deleteImage', array('uses' => 'ImagesAdsController@deleteImage'))->name('deleteImage')->middleware('admin');
Route::post('updateImage', array('uses' => 'ImagesAdsController@updateImage'))->name('updateImage')->middleware('admin');
